jQuery(function () {
    var e = $(".js-menu"),
        t = $(".js-global-nav"),
        n = $(".js-global-header"),
        r = $(".js-site-wrapper, .js-global-footer, .js-global-header"),
        i = $(".js-global-container"),
        s = $(".js-global-nav-overlay"),
        o = 0,
        u = !1;


    $(window).scroll(function (e) {
        var t = $(this).scrollTop();
        t > 85 ? t > o ? (n.css({position: "absolute", top: "0px"}).removeClass("down-page"), u = !1) : u || (n.css({position: "fixed", top: "-84px"}).addClass("down-page").animate({top: 0}, 200), u = !0) : n.removeClass("down-page"), o = t
    }), s.on("click", function (t) {
        t.preventDefault(), e.click()
    }), e.on("click", function (e) {
        e.preventDefault(), r.hasClass("menu-showing") ? ($("body").removeClass("noverflow"), $(".js-global-footer").removeClass("hidden"), s.fadeOut(100, function () {
            r.animate({right: "auto"}, 50, function () {
                t.hide(), r.removeClass("menu-showing")
            })
        })) : (t.show(), $("body, .js-global-container").addClass("noverflow"), i.hasClass("web") || $(".js-global-footer").addClass("hidden"), s.fadeIn(100, function () {
            r.animate({right: "-280px"}, 50, function () {
                r.addClass("menu-showing")
            })
        }))
    });
});