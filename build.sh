rm -rf build

mkdir build
mkdir -p build/css
mkdir -p build/js


#
#   Render Templates
#
swig render index.swig > build/index.html
swig render cities.swig > build/cities.html
swig render about.swig > build/about.html
swig render sign-in.swig > build/sign-in.html
swig render sign-up.swig > build/sign-up.html
swig render legal.swig > build/legal.html
swig render drivers.swig > build/drivers.html


#
#   Build Less Files
#
cd css
lessc -x style.less > ../build/css/style.css
cd ..

#
#   Build Javascript
#
cd js
sh build.sh > ../build/js/main.js
cd ..

#
#   Copy to build directory
#
cp -r images build
cp -r fonts build

